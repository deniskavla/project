/ *!
* imagesLoaded PACKAGED v4.1.4
* JavaScript - это что-то вроде "Вы уже сделали снимки или что?"
* Лицензия MIT
* /

/ **
* EvEmitter v1.1.0
* Lil 'источник событий
* Лицензия MIT
* /

/ * jshint не используется: правда, undef: правда, строгий: правда * /

(функция (глобальная, заводская) {
    // определение универсального модуля
    / * строгий jshint: false * / / * глобальные определения, модуль, окно * /
    if (typeof define == 'function' && define.amd) {
        // AMD - RequireJS
        определить («ev-эмиттер / ev-эмиттер», завод);
    } else if (typeof module == 'object' && module.exports) {
        // CommonJS - Browserify, Webpack
        module.exports = factory ();
    } еще {
        // Глобалы браузера
        global.EvEmitter = factory ();
    }

} (typeof window! = 'undefined'? window: this, function () {



    function EvEmitter () {}

    var proto = EvEmitter.prototype;

    proto.on = function (eventName, listener) {
        if (! eventName ||! listener) {
            возвращение;
        }
        // установить хеш событий
        var events = this._events = this._events || {};
        // установить массив слушателей
        var listeners = events [eventName] = events [eventName] || [];
        // добавляем только один раз
        if (listeners.indexOf (listener) == -1) {
            listeners.push (слушатель);
        }

        верни это;
    };

    proto.once = function (eventName, listener) {
        if (! eventName ||! listener) {
            возвращение;
        }
        // добавить событие
        this.on (eventName, слушатель);
        // установить один раз флаг
        // устанавливаем хэш
        var OnceEvents = this._onceEvents = this._onceEvents || {};
        // установить объект OnceListeners
        var OnceListeners = OnceEvents [eventName] = OnceEvents [EventName] || {};
        // установить флаг
        OnceListeners [listener] = true;

        верни это;
    };

    proto.off = function (eventName, listener) {
        var listeners = this._events && this._events [eventName];
        if (! listeners ||! listeners.length) {
            возвращение;
        }
        var index = listeners.indexOf (listener);
        if (index! = -1) {
            listeners.splice (index, 1);
        }

        верни это;
    };

    proto.emitEvent = function (eventName, args) {
        var listeners = this._events && this._events [eventName];
        if (! listeners ||! listeners.length) {
            возвращение;
        }
        // копировать, чтобы избежать помех, если .off () в слушателе
        listeners = listeners.slice (0);
        args = args || [];
        // один раз
        var OnceListeners = this._onceEvents && this._onceEvents [eventName];

        for (var i = 0; i <listeners.length; i ++) {
            var listener = listeners [i]
            var isOnce = OnceListeners && OnceListeners [слушатель];
            if (isOnce) {
                // удалить слушателя
                // удалить перед триггером, чтобы предотвратить рекурсию
                this.off (eventName, слушатель);
                // сбросить один раз флаг
                удалить OnceListeners [слушатель];
            }
            // триггер слушателя
            listener.apply (это, аргументы);
        }

        верни это;
    };

    proto.allOff = function () {
        удалить this._events;
        удалить this._onceEvents;
    };

    вернуть EvEmitter;

}));

/ *!
* imagesLoaded v4.1.4
* JavaScript - это что-то вроде "Вы уже сделали снимки или что?"
* Лицензия MIT
* /

(функция (окно, фабрика) {'использовать строгий';
    // определение универсального модуля

    / * глобальное определение: ложь, модуль: ложь, требование: ложь * /

    if (typeof define == 'function' && define.amd) {
        // AMD
        определить ([
      «Эв-эмиттер / эв-эмиттер»
    ], функция (EvEmitter) {
            возврат фабрики (окно, EvEmitter);
        });
    } else if (typeof module == 'object' && module.exports) {
        // CommonJS
        module.exports = factory (
            окно,
            требуется ( 'EV-эмиттер')
        );
    } еще {
        // глобальный браузер
        window.imagesLoaded = factory (
            окно,
            window.EvEmitter
        );
    }

}) (typeof window! == 'undefined'? window: это,

// -------------------------- завод --------------------- ----- //

    фабрика функций (окно, EvEmitter) {



    var $ = window.jQuery;
    var console = window.console;

// -------------------------- хелперы --------------------- ----- //

// расширяем объекты
    расширение функции (а, б) {
        для (вар проп в б) {
            a [prop] = b [prop];
        }
        вернуть;
    }

    var arraySlice = Array.prototype.slice;

// превращаем элемент или nodeList в массив
    function makeArray (obj) {
        if (Array.isArray (obj)) {
            // использовать объект, если уже массив
            вернуть объект;
        }

        var isArrayLike = typeof obj == 'object' && typeof obj.length == 'number';
        if (isArrayLike) {
            // преобразовать nodeList в массив
            return arraySlice.call (obj);
        }

        // массив одного индекса
        возврат [объект];
    }

// -------------------------- Изображения загружены --------------------- ----- //

    / **
    * @param {Array, Element, NodeList, String} elem
    * @param {Object или Function} опции - если функция, используется как обратный вызов
    * @param {Function} onAlways - функция обратного вызова
    * /
    функция ImagesLoaded (elem, options, onAlways) {
        // принудительно заставить ImagesLoaded () без нового, чтобы быть новым ImagesLoaded ()
        if (! (этот экземпляр ImagesLoaded)) {
            вернуть новые ImagesLoaded (elem, options, onAlways);
        }
        // используем elem как строку выбора
        var queryElem = elem;
        if (typeof elem == 'string') {
            queryElem = document.querySelectorAll (elem);
        }
        // залог, если плохой элемент
        if (! queryElem) {
            console.error ('Плохой элемент для imagesLoaded' + (queryElem || elem));
            возвращение;
        }

        this.elements = makeArray (queryElem);
        this.options = extend ({}, this.options);
        // смещаем аргументы, если не заданы параметры
        if (typeof options == 'function') {
            onAlways = опции;
        } еще {
            расширить (это. опции, опции);
        }

        if (onAlways) {
            this.on ('всегда', всегда);
        }

        this.getImages ();

        if ($) {
            // добавляем jQuery отложенный объект
            this.jqDeferred = new $ .Deferred ();
        }

        // HACK проверяем асинхронность, чтобы дать время привязать слушателей
        setTimeout (this.check.bind (this));
    }

    ImagesLoaded.prototype = Object.create (EvEmitter.prototype);

    ImagesLoaded.prototype.options = {};

    ImagesLoaded.prototype.getImages = function () {
        this.images = [];

        // фильтруем и находим предметы, если у нас есть селектор
        this.elements.forEach (this.addElementImages, this);
    };

    / **
    * @param {Node} элемент
    * /
    ImagesLoaded.prototype.addElementImages = function (elem) {
        // фильтр братьев и сестер
        if (elem.nodeName == 'IMG') {
            this.addImage (elem);
        }
        // получаем фоновое изображение на элементе
        if (this.options.background === true) {
            this.addElementBackgroundImages (elem);
        }

        // найти детей
        // нет неэлементных узлов, # 143
        var nodeType = elem.nodeType;
        if (! nodeType ||! elementNodeTypes [nodeType]) {
            возвращение;
        }
        var childImgs = elem.querySelectorAll ('img');
        // concat childElems в массив filterFound
        for (var i = 0; i <childImgs.length; i ++) {
            var img = childImgs [i];
            this.addImage (img);
        }

        // получаем дочерние фоновые изображения
        if (typeof this.options.background == 'string') {
            var children = elem.querySelectorAll (this.options.background);
            для (i = 0; i <children.length; i ++) {
                var child = children [i];
                this.addElementBackgroundImages (child);
            }
        }
    };

    var elementNodeTypes = {
        1: правда,
        9: правда,
        11: правда
    };

    ImagesLoaded.prototype.addElementBackgroundImages = function (elem) {
        var style = getComputedStyle (elem);
        if (! style) {
            // Firefox возвращает ноль, если в скрытом iframe https://bugzil.la/548397
            возвращение;
        }
        // получить URL внутри URL ("...")
        var reURL = /url\((['"])?(.*?)\1\)/gi;
        var соответствия = reURL.exec (style.backgroundImage);
        while (соответствует! == пусто) {
            var url = совпадения && совпадения [2];
            if (url) {
                this.addBackground (url, elem);
            }
            match = reURL.exec (style.backgroundImage);
        }
    };

    / **
    * @param {Image} img
    * /
    ImagesLoaded.prototype.addImage = function (img) {
        var loadingImage = новый LoadingImage (img);
        this.images.push (loadingImage);
    };

    ImagesLoaded.prototype.addBackground = function (url, elem) {
        var background = new Background (url, elem);
        this.images.push (фон);
    };

    ImagesLoaded.prototype.check = function () {
        var _this = this;
        this.progressedCount = 0;
        this.hasAnyBroken = false;
        // завершаем, если нет изображений
        if (! this.images.length) {
            this.complete ();
            возвращение;
        }

        function onProgress (изображение, элемент, сообщение) {
            // HACK - Chrome вызывает событие до изменения свойств объекта. # 83
            setTimeout (function () {
                _this.progress (изображение, элемент, сообщение);
            });
        }

        this.images.forEach (function (loadingImage) {
            loadingImage.once ('progress', onProgress);
            loadingImage.check ();
        });
    };

    ImagesLoaded.prototype.progress = function (image, elem, message) {
        this.progressedCount ++;
        this.hasAnyBroken = this.hasAnyBroken || ! Image.isLoaded;
        // событие прогресса
        this.emitEvent ('progress', [this, image, elem]);
        if (this.jqDeferred && this.jqDeferred.notify) {
            this.jqDeferred.notify (this, изображение);
        }
        // проверяем, завершено ли
        if (this.progressedCount == this.images.length) {
            this.complete ();
        }

        if (this.options.debug && console) {
            console.log ('progress:' + message, image, elem);
        }
    };

    ImagesLoaded.prototype.complete = function () {
        var eventName = this.hasAnyBroken? 'fail': 'done';
        this.isComplete = true;
        this.emitEvent (eventName, [this]);
        this.emitEvent ('always', [this]);
        if (this.jqDeferred) {
            var jqMethod = this.hasAnyBroken? 'отклонить': 'разрешить';
            this.jqDeferred [jqMethod] (this);
        }
    };

// -------------------------- ---------------------- ---- //

    function LoadingImage (img) {
        this.img = img;
    }

    LoadingImage.prototype = Object.create (EvEmitter.prototype);

    LoadingImage.prototype.check = function () {
        // Если complete - true, и браузер поддерживает натуральные размеры,
        // пытаемся проверить статус изображения вручную.
        var isComplete = this.getIsImageComplete ();
        if (isComplete) {
            // отчет основан на натуральной ширине
            this.confirm (this.img.naturalWidth! == 0, 'naturalWidth');
            возвращение;
        }

        // Если ни одна из проверок не совпадает, имитируем загрузку отдельного элемента.
        this.proxyImage = new Image ();
        this.proxyImage.addEventListener ('load', this);
        this.proxyImage.addEventListener ('ошибка', это);
        // привязать к изображению также для Firefox. # 191
        this.img.addEventListener ('load', this);
        this.img.addEventListener ('ошибка', это);
        this.proxyImage.src = this.img.src;
    };

    LoadingImage.prototype.getIsImageComplete = function () {
        // проверка на ненулевое, неопределенное naturalWidth
        // исправляет ошибку Safari + InfiniteScroll + Masonry infinite-scroll # 671
        вернуть this.img.complete && this.img.naturalWidth;
    };

    LoadingImage.prototype.confirm = function (isLoaded, message) {
        this.isLoaded = isLoaded;
        this.emitEvent ('progress', [this, this.img, message]);
    };

// ----- Мероприятия ----- //

// вызвать указанный обработчик для типа события
    LoadingImage.prototype.handleEvent = function (event) {
        var method = 'on' + event.type;
        if (этот [метод]) {
            этот [метод] (событие);
        }
    };

    LoadingImage.prototype.onload = function () {
        this.confirm (true, 'onload');
        this.unbindEvents ();
    };

    LoadingImage.prototype.onerror = function () {
        this.confirm (false, 'onerror');
        this.unbindEvents ();
    };

    LoadingImage.prototype.unbindEvents = function () {
        this.proxyImage.removeEventListener ('load', this);
        this.proxyImage.removeEventListener ('ошибка', это);
        this.img.removeEventListener ('load', this);
        this.img.removeEventListener ('ошибка', это);
    };

// -------------------------- Задний план --------------------- ----- //

    Функция Background (url, element) {
        this.url = url;
        this.element = element;
        this.img = new Image ();
    }

// наследуем прототип LoadingImage
    Background.prototype = Object.create (LoadingImage.prototype);

    Background.prototype.check = function () {
        this.img.addEventListener ('load', this);
        this.img.addEventListener ('ошибка', это);
        this.img.src = this.url;
        // проверяем, завершено ли изображение
        var isComplete = this.getIsImageComplete ();
        if (isComplete) {
            this.confirm (this.img.naturalWidth! == 0, 'naturalWidth');
            this.unbindEvents ();
        }
    };

    Background.prototype.unbindEvents = function () {
        this.img.removeEventListener ('load', this);
        this.img.removeEventListener ('ошибка', это);
    };

    Background.prototype.confirm = function (isLoaded, message) {
        this.isLoaded = isLoaded;
        this.emitEvent ('progress', [this, this.element, message]);
    };

// -------------------------- jQuery --------------------- ----- //

    ImagesLoaded.makeJQueryPlugin = function (jQuery) {
        jQuery = jQuery || window.jQuery;
        if (! jQuery) {
            возвращение;
        }
        // установить локальную переменную
        $ = jQuery;
        // $ (). imagesLoaded ()
        $ .fn.imagesLoaded = function (options, callback) {
            var instance = new ImagesLoaded (this, options, callback);
            return instance.jqDeferred.promise ($ (this));
        };
    };
// попробуйте сделать плагин
    ImagesLoaded.makeJQueryPlugin ();

// -------------------------- ---------------------- ---- //

    вернуть ImagesLoaded;

});