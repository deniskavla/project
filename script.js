const tabs = Array.from(document.getElementsByClassName('tabs-title'));
const tabsDescr = Array.from(document.getElementsByClassName('tab-descr'));
document.querySelector('.header-servisec').addEventListener('click', function (event) {
    if (event.target.classList.contains('tabs-title')) {
        tabs.forEach(item => item.classList.remove('active'));
        event.target.classList.add('active');
        tabsDescr.forEach(item => {
            item.classList.remove('active');
            if (event.target.dataset.tab === item.dataset.tab)
                item.classList.add('active')
        })
    }
});

// const portfolioHover = {
//     hover: "<div class='portfolio-hover hov-img'> " +
//     "<div class='portfolio-links'>" +
//     "<a href='#'>" +
//     "<i class='fas fa-link'></i>" +
//     "</a>" +
//     "<a href='#'>" +
//     "<i class='fas fa-search'></i>" +
//     "</a>" +
//     "</div>" +
//     "<div class='portfolio-info'>" +
//     "<p>creative design</p>" +
//     "</div>" +
//     "</div>",
// };


const images = {
    type1: [
        {

            url: "<li class='portfolio-img'>" +
            "<img src='./img/graphic%20design/graphic-design1.jpg' style='width: 286px; height: 208px'>" +
            "<div class='portfolio-hover hov-img'>" +
            "   <div class='portfolio-links'>" +
            "<a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 1',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design2.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 2',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design3.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 3',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design4.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 4',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design5.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 5',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design6.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 6',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design7.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 7',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design8.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 8',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design9.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 9',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design10.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 10',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design11.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 11',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design12.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 12',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design8.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 1 Image 13',
            link: '',
        },
    ],
    type2: [
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design1.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 1',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design2.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 2',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design3.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 3',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design4.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 4',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design5.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 5',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design6.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 6',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design7.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 7',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design8.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 8',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design9.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 9',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design10.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 10',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design11.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 11',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design12.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 12',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design13.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 13',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design14.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 14',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design15.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 15',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/web%20design/web-design16.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 2 Image 16',
            link: '',
        },
    ],
    type3: [
        {
            url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page1.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 3 Image 1',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page2.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 3 Image 2',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page3.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 3 Image 3',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page4.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 3 Image 4',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page5.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 3 Image 5',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page6.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 3 Image 6',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page7.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 3 Image 7',
            link: '',
        },

    ],
    type4: [
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress1.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 1',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress2.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 2',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress3.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 3',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress4.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 4',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress5.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 5',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress6.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 6',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress7.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 7',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress8.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 8',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress9.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 9',
            link: '',
        },
        {
            url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress10.jpg'" +
            "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
            name: 'Type 4 Image 10',
            link: '',
        },

    ],
};
const TYPES = {
    all: 'all',
    type1: 'type1',
    type2: 'type2',
    type3: 'type3',
    type4: 'type4',
};
const liActiveClass = 'tabs-service-active';
const typeContainer = document.querySelector('.amazing-ul');
const imageContainer = document.querySelector('.images');
const loadMore = document.querySelector('.load-more');
let activeType = TYPES.all;
let perPage = 12;
let activePage = 0;

document.querySelector('.tabs-service[data-service=all]').classList.add(liActiveClass);
typeContainer.addEventListener('click', e => {
    const {target} = e;
    const liList = document.querySelectorAll('.tabs-service');
    liList.forEach(li => li.classList.remove(liActiveClass));
    target.classList.add(liActiveClass);
    if (activeType !== target.dataset.service) {
        activeType = target.dataset.service;
        activePage = 0;
        addimg(target.dataset.service);
    }
});
let lodeQuantity = 0;

function addimg(type) {
    if (images[type]) {
        activePage = activePage + 1;
        const imageByType = [...images[type]].slice(0, activePage * perPage);
        imageContainer.innerHTML = imageByType.map(image => image.url).join('');
    } else {
        activePage = activePage + 1;
        const concatedImages = [];
        Object.keys(images).forEach(key => concatedImages.push(...images[key]));
        const imageByType = [...concatedImages].slice(0, activePage * perPage);
        imageContainer.innerHTML = imageByType.map(image => image.url).join('');
    }
}


loadMore.addEventListener('click', () => {
    $('.load-more').hide()
    $('.container').show();
    setTimeout(() => {
        addimg(activeType);
        $('.load-more').show();
        if (lodeQuantity === 3) {
            $('.load-more').hide()
        }
        $('.container').hide();

    }, 2000);
    ++lodeQuantity;
});

addimg(activeType);


$('.images > li').hover(
    function () {
        $(this).append($(".portfolio-hover").css("opacity", "1"));
        $(this).append($(".portfolio-hover").css("transition", "opacity 0.4s"));
        // $(this).append($(".portfolio-hover").css("transform", "translateY(0)"));
    },
    function () {
        $(this).append($(".portfolio-hover").css("opacity", "0","transform", "translateY(-100%)"));

    }
);


$('.list img').click(function () {
    selectPerson($(this));
});

function selectPerson($el) {
    $('.selected')
        .empty()
        .append(`
                 <div data-id="${$el.data('id')}">${$el.data('article')}</div>
                 <h2>${$el.data('name')}</h2>
                 <p>${$el.data('position')}</p>
                 <img src="${$el.attr('src')}"/>
                `)
}

$('.img-foot').first().click();

$('.buttom-people-right').click(function (event) {
    let id = $('.selected > div').data('id');
    id++;
    if (id > 3) {
        id = 0;
    }
    selectPerson($(`.img-foot[data-id=${id}]`));
});

$('.buttom-people-left').click(function (event) {
    let id = $('.selected > div').data('id');
    id--;
    if (id === -1) {
        id = 3;
    }
    selectPerson($(`.img-foot[data-id=${id}]`));
});

























